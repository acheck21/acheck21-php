<?php

/**
* Copyright (c) 2015 Diversified Check Solutions, LLC
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/


/**
 * This class is used to hold data that is transmitted in an ACHeck21 batch file.
 * Its purpose is to simplify converting the data into xml to
 * make a call to the SendBatch REST service. 
 */
class ACHeck21Batch 
{
	private $transactions;
	private $transactionsNode;
	private $entryCount;
	private $totalAmount;
	
	/**
	* Constructs an ACHeck21Batch
	*
	*/
	public function __construct($clientID, $seqNbr) 
	{
		if (is_null($clientID) || is_null($seqNbr))
		{
			throw new Exception("Missing required argument");
		}

		$this->transactions = new SimpleXMLElement("<ACHFile></ACHFile>");
		$this->transactions->addChild("ClientID", $clientID);
		$this->transactions->addChild("UploadDate", date('Y/m/d'));
		$this->transactions->addChild("SeqNbr", $seqNbr);
		$this->transactionsNode = $this->transactions->addChild("Transactions");
		$this->transactions->addChild("EntryCount");
		$this->transactions->addChild("XmitTotal");
		$this->entryCount = 0;
		$this->totalAmount = 0;
	}

	/**
	* Adds a transaction to the batch
	*/
	public function addTransaction($clientTag, 
								   $entryClass, 
								   $transactionCode, 
								   $transitNumber, 
								   $ddaNumber, 
								   $checkNumber, 
								   $checkAmount, 
								   $individualName = NULL,
								   $companyName = NULL,
								   $companyEntryDescription = NULL,
								   $phoneNumber = NULL,
								   $dlNumber = NULL,
								   $addendum = NULL,
								   $frontImageFilename = NULL,
								   $rearImageFilename = NULL,
								   $rccLine1 = NULL,
								   $rccLine2 = NULL,
								   $rccLine3 = NULL,
								   $rccLine4 = NULL,
								   $rccLine5 = NULL,
								   $rccSignature = NULL,
								   $rccEndorse1 = NULL,
								   $rccEndorse2 = NULL,
								   $rccEndorse3 = NULL,
								   $rccEndorse4 = NULL)
	{
		if (is_null($clientTag) || is_null($entryClass) || is_null($transactionCode) ||
			is_null($transitNumber) || is_null($ddaNumber) || is_null($checkNumber) || 
			is_null($checkAmount))
		{
			throw new Exception("Required transaction property missing");
		}

		$node = $this->transactionsNode->addChild("Transaction");
		$node->addChild("ClientTag", $clientTag);
		$node->addChild("EntryClassCd", $entryClass);
		$node->addChild("TransactionCd", $transactionCode);
		$node->addChild("TransitNbr", $transitNumber);
		$node->addChild("DDANbr", $ddaNumber);
		$node->addChild("CheckNbr", $checkNumber);
		$node->addChild("CheckAmt", $checkAmount);

		if (!is_null($individualName))
		{
			$node->addChild("IndividualName", $individualName);
		}
		if (!is_null($companyName))
		{
			$node->addChild("CompanyName", $companyName);
		}
		if (!is_null($companyEntryDescription))
		{
			$node->addChild("CompanyEntryDescr", $companyEntryDescription);
		}
		if (!is_null($phoneNumber))
		{
			$node->addChild("PhoneNumber", $phoneNumber);
		}
		if (!is_null($dlNumber))
		{
			$node->addChild("DLNumber", $dlNumber);
		}
		if (!is_null($addendum))
		{
			$node->addChild("Addenda")->addChild("Addendum", $addendum);
		}
		else
		{
			$node->addChild("Addenda");
		}
		if (!is_null($frontImageFilename) && !is_null($rearImageFilename))
		{
			$data = file_get_contents($frontImageFilename);
			$node->addChild("FrontImage", base64_encode($data));

			$data = file_get_contents($rearImageFilename);
			$node->addChild("RearImage", base64_encode($data));
		}

		if (!is_null($rccLine1) || !is_null($rccSignature) || !is_null($rccEndorse1))
		{
			$rccNode = $node->addChild("RCCInfo");
			if (!is_null($rccLine1))
			{
				$rccNode->addChild("Line1", $rccLine1);
			}
			if (!is_null($rccLine2))
			{
				$rccNode->addChild("Line2", $rccLine2);
			}
			if (!is_null($rccLine3))
			{
				$rccNode->addChild("Line3", $rccLine3);
			}
			if (!is_null($rccLine4))
			{
				$rccNode->addChild("Line4", $rccLine4);
			}
			if (!is_null($rccLine5))
			{
				$rccNode->addChild("Line5", $rccLine5);
			}

			if (!is_null($rccSignature))
			{
				$rccNode->addChild("Signature", $rccSignature);
			}

			if (!is_null($rccEndorse1))
			{
				$rccNode->addChild("Endorse1", $rccEndorse1);
			}
			if (!is_null($rccEndorse2))
			{
				$rccNode->addChild("Endorse2", $rccEndorse2);
			}
			if (!is_null($rccEndorse3))
			{
				$rccNode->addChild("Endorse3", $rccEndorse3);
			}
			if (!is_null($rccEndorse4))
			{
				$rccNode->addChild("Endorse4", $rccEndorse4);
			}
		}
		$this->entryCount++;
		$this->totalAmount += $checkAmount;
	}
	/**
	* Serializes the data into xml. Suitable for a call to the ACHeck21 SendBatch REST service. 
	* However, it would still need to be Base64 encoded. 
	*/
	public function toXMLString()
	{
		$this->transactions->EntryCount = $this->entryCount;
		$this->transactions->XmitTotal = number_format($this->totalAmount, 2);
		$dom = dom_import_simplexml($this->transactions);
		return $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);
	}

}