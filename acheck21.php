<?php
/**
* Copyright (c) 2015 Diversified Check Solutions, LLC
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
require_once 'vendor/autoload.php';

/**
* This class wraps the ACHeck21 rest API to provide an easy to use PHP interface. 
* All functions are static and stateless. See the REST service documentation at 
* http://help.acheck21.com/hc/en-us/categories/200315315-Development-Center
*/
class ACHeck21
{
	private static $baseUrl = 'https://gateway.acheck21.com/GlobalGateway/rest';

	/**
	* Creates a check in the ACHeck21 global gateway. It does this by 
	* making a call to the ACHeck21 CreateCheck REST service. 
	* For more details on the REST service, see http://help.acheck21.com/hc/en-us/articles/203384609-Web-Service-CreateCheck
	* @return The id of the created check. 
	*/
	public static function createCheck($username, 
                                   	   $password, 
	                                   $clientID, 
	                                   $checkNumber,
	                                   $transitNumber,
	                                   $ddaNumber,
	                                   $accountType,
	                                   $checkAmount,
	                                   $entryClass,
	                                   $clientTag = NULL, 
	                                   $individualName = NULL, 
	                                   $frontImageFilename = NULL,
	                                   $rearImageFilename = NULL,
	                                   $micr = NULL,
	                                   $postingDate = NULL,
	                                   $addendum = NULL)
	{
		// Check to make sure all of the required fields are present and not null
		if (is_null($username) || is_null($password) || is_null($clientID) || is_null($checkNumber) || is_null($transitNumber) ||
			is_null($ddaNumber) || is_null($accountType) || is_null($checkAmount) || is_null($entryClass))
		{
			throw new Exception('Missing required field(s) to create a check');
		}

		// Create the http body from the required fields for the REST service call
		$body = array("ClientID" => $clientID, 
			           "CheckNumber" => $checkNumber, 
			           "TransitNumber" => $transitNumber, 
			           "DDANumber" => $ddaNumber, 
			           "AccountType" => $accountType,
			           "CheckAmount" => $checkAmount,
			           "EntryClass" => $entryClass);

		$headers = array();

		// Go through the optional paramaters. If present, add each one to the body. 
		if (!is_null($clientTag))
		{
			$body["ClientTag"] = $clientTag;
		}
		if (!is_null($individualName))
		{
			$body["IndividualName"] = $individualName;
		}
		if (!is_null($micr))
		{
			$body["MICR"] = $micr;
		}
		if (!is_null($postingDate))
		{
			$body["PostingDate"] = date('Y-m-d', $postingDate);
		}
		if (!is_null($addendum))
		{
			$body["Addendum"] = $addendum;
		}
		if (!is_null($frontImageFilename))
		{
			$data = file_get_contents($frontImageFilename);
			$body["FrontImage"] = base64_encode($data);
		}
		if (!is_null($rearImageFilename))
		{
			$data = file_get_contents($rearImageFilename);
			$body["RearImage"] = base64_encode($data);
		}

		// Set up basic auth
		Unirest\Request::auth($username, $password);

		// Make the request
		$response = Unirest\Request::post(self::$baseUrl . '/check', $headers, $body);
		if ($response->code == 200)
		{

			$node = simplexml_load_string($response->body);
			$path = parse_url($node[0], PHP_URL_PATH);
			$pathArray = explode('/', $path);
			return $pathArray[count($pathArray) - 1];
		}
		var_dump($response);
		return null;
	}

	/**
     * Finds and returns a list of Checks in the global gateway based on some search criteria. 
     * If the checks haven't been processed, you will need to use findPendingChecksDetails to find them.
     * 
     * This function wraps a call to the FindChecksDetails REST service. Documented at 
     * http://help.acheck21.com/hc/en-us/articles/203285679-Web-Service-FindChecksDetails
     */
	public static function findChecksDetails($username, $password, $clientID, $query)
	{
		$url = self::$baseUrl . "/checks/" . $clientID . "/details?" . $query;
		return self::checksQuery($url, $username, $password);
	}

	 /**
     * Finds and returns a list of pending checks in the global gateway based on some search criteria. 
     * If the checks have been processed, you will need to use findChecksDetails to find them.
     * 
     * This function wraps a call to the FindPendingChecksDetails REST service. Documented at 
     * http://help.acheck21.com/hc/en-us/articles/203568379-Web-Service-FindPendingChecksDetails
     */
	public static function findPendingChecksDetails($username, $password, $clientID, $query)
	{
		$url = self::$baseUrl . "/checks/" . $clientID . "/pending/details?" . $query;
		return self::checksQuery($url, $username, $password);
	}

	 /**
     * Deletes a check from the global gateway.
     * 
     * This function wraps a call to the DeleteCheck REST service. Documented at
     * http://help.acheck21.com/hc/en-us/articles/204501455-Web-Service-DeleteCheck
     * 
     */
	public static function deleteCheck($username, $password, $checkID)
	{
		Unirest\Request::auth($username, $password);

		$response = Unirest\Request::delete(self::$baseUrl . '/check/' . $checkID);
		return ($response->code == 204);
	}
	/**
     * Sends a batch to the global gateway. Uses a data object to represent the data contained in a batch file
     * and serializes that into the proper format required by the global gateway. 
     * 
     * This function wraps a call to the SendBatch REST service. Documented at
     * http://help.acheck21.com/hc/en-us/articles/204252565-Web-Service-SendBatch
     */
	public static function sendBatch($username, $password, $clientID, $batch)
	{
		Unirest\Request::auth($username, $password);
		$batchXml = $batch->toXMLString();

		$body = array("Base64EncodedBatch" => base64_encode($batchXml),
			          "Filename" => "batch.xml");

		$response = Unirest\Request::post(self::$baseUrl . "/batch/" . $clientID, array(), $body);
		return ($response->code == 204);
	}


	private static function checksQuery($url, $username, $password)
	{
		Unirest\Request::auth($username, $password);

		$response = Unirest\Request::get($url);
		if ($response->code == 204)
		{
			return array();
		}
		else if ($response->code == 200)
		{
			$node = simplexml_load_string($response->body);
			return $node;
		}
		return null;
	}
}