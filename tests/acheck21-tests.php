<?php

require '../acheck21.php';
require_once '../acheck21-batch.php';
require_once '../vendor/autoload.php';


class ACHeck21Test extends PHPUnit_Framework_TestCase
{
	protected function setUp()
	{
		$propStr = file_get_contents('test-properties.json');
		$props = json_decode($propStr);


		$this->username = $props->username;
		$this->password = $props->password;
		$this->clientid = $props->clientid;
	}
	public function testSimpleCheck()
	{
		
		$transitNumber = "063100277";
        $ddaNumber = "12345678912345";
        $accountType = "Checking";
        $checkAmount = "132.10";
        $entryClass = "WEB";
        $this->checkTest($transitNumber,
	                           $ddaNumber,
	                           $accountType,
	                           $checkAmount,
	                           $entryClass);

	}
	public function testCheckWithPostingDate()
	{
		$transitNumber = "063100277";
        $ddaNumber = "12345678912345";
        $accountType = "Checking";
        $checkAmount = "132.10";
        $entryClass = "WEB";
        $postingDate = time() + (24 * 60 * 60);

        $this->checkTest($transitNumber,
	                           $ddaNumber,
	                           $accountType,
	                           $checkAmount,
	                           $entryClass, NULL, NULL, NULL, NULL, NULL, $postingDate);
	}

	public function testCheckWithImages()
	{
		$transitNumber = "063100277";
        $ddaNumber = "12345678912345";
        $accountType = "Checking";
        $checkAmount = "132.10";
        $entryClass = "C21";
        $postingDate = time() + (24 * 60 * 60);

        $this->checkTest($transitNumber,
	                           $ddaNumber,
	                           $accountType,
	                           $checkAmount,
	                           $entryClass, NULL, NULL, 'test-images/front.tiff', 'test-images/back.tiff', NULL, $postingDate);
	}

	public function testFindChecksDetails()
	{
		$results = ACHeck21::findChecksDetails($this->username, $this->password, $this->clientid, "CheckID=equal,80256940");

		$checkInfo = $results->CheckInfo;
		$this->assertFalse(is_array($checkInfo));
		$this->assertEquals("80256940", $checkInfo->CheckID);
		$this->assertEquals("2015-04-29", $checkInfo->UploadDate);
		$this->assertEquals("TestAcct2", $checkInfo->IndividualName);
		$this->assertEquals("12358785757", $checkInfo->CheckNumber);
		$this->assertEquals("012345672", $checkInfo->TransitNumber);
		$this->assertEquals("1239873995", $checkInfo->DDANumber);
		$this->assertEquals("Checking", $checkInfo->AccountType);
		$this->assertEquals("2.00", $checkInfo->CheckAmount);
		$this->assertEquals("255,2,0", $checkInfo->ClientTag);
		$this->assertEquals("937", $checkInfo->EntryClass);
		$this->assertEquals("2015-04-29", $checkInfo->PostingDate);
		$this->assertEquals("2015-04-29", $checkInfo->Presentments->Presentment->ToFedDate);
	}

	public function testFindPendingChecksDetails()
	{
		$checkNumber = '' . rand(0, 100000);
		$transitNumber = "063100277";
        $ddaNumber = "12345678912345";
        $accountType = "Checking";
        $checkAmount = "132.10";
        $entryClass = "WEB";

		$checkID = ACHeck21::createCheck($this->username, $this->password, $this->clientid, 
							  $checkNumber, $transitNumber, $ddaNumber, 
							  $accountType, $checkAmount, $entryClass);

		$results = ACHeck21::findPendingChecksDetails($this->username, $this->password, $this->clientid, "CheckID=equal," . $checkID);

		$checkInfo = $results->CheckInfo;
		$this->assertFalse(is_array($checkInfo));
		$this->assertEquals($checkID, $checkInfo->CheckID);
		$this->assertTrue(ACHeck21::deleteCheck($this->username, $this->password, $checkID));
	}

	public function testSendBatch()
	{
		$seqNbr = rand(1, 1000000);	
		$batch = new ACHeck21Batch($this->clientid, $seqNbr);
		$transitNumber = "063100277";
        $ddaNumber = "12345678912345";
        $accountType = "Checking";
        $checkAmount = 132.10;
		$batch->addTransaction('poo' . $seqNbr, "WEB", 27, $transitNumber, $ddaNumber, rand(1, 1000000), $checkAmount, 
			                   "Nobody J. Smith", "Acme Inc.", "Plop");
		$batch->AddTransaction('poo1' . $seqNbr, "WEB", 27, $transitNumber,$ddaNumber, rand(1, 1000000), $checkAmount + 20, 
			                   "Nobody J. Smith", "Acme Inc.", "Plop");

		$this->assertTrue(ACHeck21::sendBatch($this->username, $this->password, $this->clientid, $batch));
	}

	private function checkTest($transitNumber,
	                           $ddaNumber,
	                           $accountType,
	                           $checkAmount,
	                           $entryClass,
	                           $clientTag = NULL, 
	                           $individualName = NULL, 
	                           $frontImageFilename = NULL,
	                           $backImageFilename = NULL,
	                           $micr = NULL,
	                           $postingDate = NULL,
	                           $addendum = NULL)
	{
		

		$checkNumber = '' . rand(0, 100000);

		$checkID = ACHeck21::createCheck($this->username, $this->password, $this->clientid, 
							  $checkNumber, $transitNumber, $ddaNumber, 
							  $accountType, $checkAmount, $entryClass,
							  $clientTag, $individualName, $frontImageFilename, 
							  $backImageFilename, $micr, $postingDate, $addendum);
		
	    $this->assertGreaterThan(0, 0 + $checkID);
	    //echo($checkID);
	    $this->assertTrue(ACHeck21::deleteCheck($this->username, $this->password, $checkID));
	}
}